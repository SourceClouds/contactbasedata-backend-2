/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.service.InternalPersonService;
import org.eclipse.openk.contactbasedata.viewmodel.InternalPersonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/internal-persons")
public class InternalPersonController {
    @Autowired
    private InternalPersonService internalPersonService;

    @GetMapping("/{contactUuid}")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiOperation(value = "Anzeigen einer internen Person")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
                            @ApiResponse(code = 404, message = "Person wurde nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public InternalPersonDto getInternalPerson(@PathVariable UUID contactUuid) {
        return internalPersonService.findInternalPerson(contactUuid);
    }

    @GetMapping
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiOperation(value = "Anzeigen von internen Personen", notes = "Filtern und Sortieren ist möglich. Interne Personen werden seitenweise geliefert. Werden mehrere Filter gleichzeit verwendet, so werden diese mit 'UND' verknüpft!")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    public Page<InternalPersonDto> readInternalPersons(
            @RequestParam( "uid") Optional<String> uid,
            @RequestParam( "userRef" ) Optional<String> userRef,
            @RequestParam( "alsoShowAnonymous" ) Optional<Boolean> alsoShowAnonymous,
            @PageableDefault(sort = {"lastName"}, direction = Sort.Direction.ASC) Pageable pageable){
            return internalPersonService.findInternalPersons(
                    alsoShowAnonymous.orElse( false ),
                    uid.orElse(null ),
                    userRef.orElse(null ),
                    pageable);
    }

    @PostMapping
    @Secured({"ROLE_KON-ADMIN", "ROLE_KON-WRITER"})
    @ApiOperation(value = "Anlegen einer internen Person")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "interne Person erfolgreich angelegt"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<InternalPersonDto> insertInternalPerson(
            @Validated @RequestBody InternalPersonDto internalPersonDto) {
        InternalPersonDto savedInternalPersonDto = internalPersonService.insertInternalPerson(internalPersonDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedInternalPersonDto.getContactUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedInternalPersonDto);
    }

    @PutMapping("/{contactUuid}")
    @Secured({"ROLE_KON-ADMIN", "ROLE_KON-WRITER"})
    @ApiOperation(value = "Ändern einer internen Person")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Interne Person wurde aktualisiert"),
            @ApiResponse(code = 400, message = "Ungültige Eingabe"),
            @ApiResponse(code = 404, message = "Nicht gefunden")})
    public ResponseEntity updateInternalPerson(@PathVariable UUID contactUuid, @Validated @RequestBody InternalPersonDto internalPersonDto) {

        if (!internalPersonDto.getContactUuid().equals(contactUuid)) {
            throw new BadRequestException("invalid.uuid.path.object");
        }

        internalPersonService.updateInternalPerson(internalPersonDto);
        return ResponseEntity.ok().build();
    }
}

