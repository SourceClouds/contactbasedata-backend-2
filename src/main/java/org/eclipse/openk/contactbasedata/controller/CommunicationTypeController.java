/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.exceptions.ConflictException;
import org.eclipse.openk.contactbasedata.service.CommunicationTypeService;
import org.eclipse.openk.contactbasedata.viewmodel.CommunicationTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/communication-types")
public class CommunicationTypeController {
    @Autowired
    private CommunicationTypeService communicationTypeService;


    @GetMapping("/{communicationTypeUuid}")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiOperation(value = "Anzeigen eines Kommunikationstyps")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
                            @ApiResponse(code = 404, message = "Person wurde nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public CommunicationTypeDto getCommunicationType(@PathVariable UUID communicationTypeUuid) {
        return communicationTypeService.findCommunicationType(communicationTypeUuid);
    }

    @GetMapping
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiOperation(value = "Anzeigen aller Kommunikationstypen")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    public List<CommunicationTypeDto> getCommunicationTypes(){
            return communicationTypeService.findCommunicationTypes();
    }

    @PostMapping
    @Secured({"ROLE_KON-ADMIN"})
    @ApiOperation(value = "Anlegen eines Kommunikationstyps")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Kommunikationstyp erfolgreich angelegt"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<CommunicationTypeDto> insertCommunicationType(
            @Validated @RequestBody CommunicationTypeDto communicationTypeDto) {
        CommunicationTypeDto savedCommunicationTypeDto = communicationTypeService.insertCommunicationType(communicationTypeDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedCommunicationTypeDto.getUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedCommunicationTypeDto);
    }

    @PutMapping("/{uuid}")
    @Secured({"ROLE_KON-ADMIN"})
    @ApiOperation(value = "Ändern eines Kommunikationstyps")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Kommunikationstyp wurde aktualisiert"),
            @ApiResponse(code = 400, message = "Ungültige Eingabe"),
            @ApiResponse(code = 404, message = "Nicht gefunden")})
    public ResponseEntity updateCommunicationType(@PathVariable UUID uuid, @Validated @RequestBody CommunicationTypeDto communicationTypeDto) {

        if (!communicationTypeDto.getUuid().equals(uuid)) {
            throw new BadRequestException("invalid.uuid.path.object");
        }

        communicationTypeService.updateCommunicationType(communicationTypeDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{uuid}")
    @Secured("ROLE_KON-ADMIN")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Kommunikationstyp löschen")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 400, message = "Ungültige Anfrage"),
            @ApiResponse(code = 404, message = "Nicht gefunden"),
            @ApiResponse(code = 409, message = "Datensatz konnte nicht gelöscht werden")})
    public void removeComunicationType(@PathVariable UUID uuid) {
        try {
            communicationTypeService.removeCommunicationType(uuid);
        }
        catch( Exception ex ) {
            throw new ConflictException("Communication type couldn't be deleted");
        }
    }
}

