/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.contactbasedata.config.auth;

import feign.Response;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.api.AuthNAuthApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

@Component
@Log4j2
public class JwtTokenValidationFilter extends OncePerRequestFilter {
    @Autowired
    private AuthNAuthApi authNAuthApi;

    @Value("${jwt.useStaticJwt}")
    private boolean useStaticJwt;

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String authenticationHeader = useStaticJwt ? null : request.getHeader(this.tokenHeader);

            if(authenticationHeader != null && !authenticationHeader.isEmpty()) {
                final String bearerTkn= authenticationHeader.replace("Bearer ", "");
                Response res = authNAuthApi.isTokenValid(bearerTkn);
                if( res.status() != HttpStatus.OK.value() ) {
                    final HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper((HttpServletResponse)response);
                    wrapper.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token expired or not valid");
                    chain.doFilter(request, wrapper.getResponse());

                    return;
                }
            }
            chain.doFilter(request, response);
    }
}
