package org.eclipse.openk.contactbasedata.exceptions;

import org.eclipse.openk.contactbasedata.enums.OperationType;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE)
public class OperationDeniedException extends RuntimeException{
    public OperationDeniedException(OperationType operation, String message) {super(message);
    }
}
