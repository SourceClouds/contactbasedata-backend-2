/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.exceptions.NotFoundException;
import org.eclipse.openk.contactbasedata.mapper.CommunicationTypeMapper;
import org.eclipse.openk.contactbasedata.model.RefCommunicationType;
import org.eclipse.openk.contactbasedata.repository.CommunicationTypeRepository;
import org.eclipse.openk.contactbasedata.viewmodel.CommunicationTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Service
public class CommunicationTypeService {

    @Value("${ldap-sync.db-id-mapping.telephone-number-id}")
    private Long telephoneNumberId;

    @Value("${ldap-sync.db-id-mapping.mail-id}")
    private Long mailId;

    @Autowired
    private CommunicationTypeRepository communicationTypeRepository;

    @Autowired
    private CommunicationTypeMapper communicationTypeMapper;


    public CommunicationTypeDto findCommunicationType(UUID communicationTypeUuid) {

        CommunicationTypeDto communicationTypeDto = communicationTypeMapper.
                        toCommunicationTypeDto(communicationTypeRepository.findByUuid(communicationTypeUuid).
                        orElseThrow(NotFoundException::new));

        if (Objects.equals(telephoneNumberId, communicationTypeDto.getId())
                || Objects.equals(mailId, communicationTypeDto.getId())) {
            communicationTypeDto.setMappingLdap(true);
        } else {
            communicationTypeDto.setEditable(true);
        }

        return communicationTypeDto;
    }

    public List<CommunicationTypeDto> findCommunicationTypes() {

        List<CommunicationTypeDto> communicationTypeDtoList = communicationTypeRepository.findAll().stream()
                .map(communicationTypeMapper::toCommunicationTypeDto)
                .collect(Collectors.toList());

        for (CommunicationTypeDto communicationTypeDto : communicationTypeDtoList) {
            if (Objects.equals(telephoneNumberId, communicationTypeDto.getId())
                    || Objects.equals(mailId, communicationTypeDto.getId())) {
                communicationTypeDto.setMappingLdap(true);
            } else {
                communicationTypeDto.setEditable(true);
            }
        }

        return communicationTypeDtoList;
    }

    @Transactional
    public CommunicationTypeDto insertCommunicationType(CommunicationTypeDto communicationTypeDto) {

        RefCommunicationType communicationTypeToSave = communicationTypeMapper.toRefCommunicationType(communicationTypeDto);
        communicationTypeToSave.setUuid(UUID.randomUUID());

        RefCommunicationType savedCommunicationType = communicationTypeRepository.save(communicationTypeToSave);

        return communicationTypeMapper.toCommunicationTypeDto(savedCommunicationType);
    }

    @Transactional
    public CommunicationTypeDto updateCommunicationType(CommunicationTypeDto communicationTypeDto){
        RefCommunicationType communicationTypeUpdated;

        RefCommunicationType existingCommunicationType = communicationTypeRepository
                .findByUuid(communicationTypeDto.getUuid())
                .orElseThrow(() -> new NotFoundException("communication.type.uuid.not.existing"));

        RefCommunicationType communicationTypeToSave = communicationTypeMapper.toRefCommunicationType(communicationTypeDto);
        communicationTypeToSave.setId(existingCommunicationType.getId());

        communicationTypeUpdated = communicationTypeRepository.save(communicationTypeToSave);

        return  communicationTypeMapper.toCommunicationTypeDto(communicationTypeUpdated);
    }

    @Transactional
    public void removeCommunicationType(UUID uuid) {
        RefCommunicationType existingCommunicationType  = communicationTypeRepository.findByUuid(uuid)
                .orElseThrow( () -> new NotFoundException("communication.type.uuid.not.existing"));

        communicationTypeRepository.delete(existingCommunicationType);
    }

}
