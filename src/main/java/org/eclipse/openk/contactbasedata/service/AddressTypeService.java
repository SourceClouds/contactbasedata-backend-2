/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.constants.Constants;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.exceptions.NotFoundException;
import org.eclipse.openk.contactbasedata.mapper.AddressTypeMapper;
import org.eclipse.openk.contactbasedata.model.RefAddressType;
import org.eclipse.openk.contactbasedata.repository.AddressTypeRepository;
import org.eclipse.openk.contactbasedata.viewmodel.AddressTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Service
public class AddressTypeService {
    @Autowired
    private AddressTypeRepository addressTypeRepository;

    @Autowired
    AddressTypeMapper addressTypeMapper;

    public List<AddressTypeDto> findAllAddressTypes() {
        return addressTypeRepository.findAll().stream()
                    .map( addressTypeMapper::toAddressTypeDto )
                    .collect(Collectors.toList());
    }


    public AddressTypeDto getAddressTypeByUuid(UUID addressTypeUuid) {
        RefAddressType refAddressType = addressTypeRepository
                .findByUuid(addressTypeUuid)
                .orElseThrow(() -> new NotFoundException(Constants.ADDRESS_TYPE_UUID_NOT_EXISTING));
        return addressTypeMapper.toAddressTypeDto(refAddressType);
    }

    @Transactional
    public AddressTypeDto insertAddressType(AddressTypeDto addressTypeDto) {
        RefAddressType addressTypeToSave = addressTypeMapper.toRefAddressType(addressTypeDto);
        addressTypeToSave.setUuid(UUID.randomUUID());

        RefAddressType savedAddressType = addressTypeRepository.save(addressTypeToSave);
        return addressTypeMapper.toAddressTypeDto(savedAddressType);
    }

    @Transactional
    public AddressTypeDto updateAddressType(AddressTypeDto addressTypeDto){
        RefAddressType addressTypeUpdated;
        RefAddressType addressTypeToSave = addressTypeMapper.toRefAddressType(addressTypeDto);
        RefAddressType existingAddressType = addressTypeRepository
                .findByUuid(addressTypeDto.getUuid())
                .orElseThrow(() -> new NotFoundException(Constants.ADDRESS_TYPE_UUID_NOT_EXISTING));
        addressTypeToSave.setId(existingAddressType.getId());
        addressTypeUpdated = addressTypeRepository.save(addressTypeToSave);

        return addressTypeMapper.toAddressTypeDto(addressTypeUpdated);
    }

    @Transactional
    public void removeAddressType(UUID uuid) {
        RefAddressType existingAddressType = addressTypeRepository.findByUuid(uuid)
                .orElseThrow( () -> new BadRequestException(Constants.ADDRESS_TYPE_UUID_NOT_EXISTING));

        addressTypeRepository.delete(existingAddressType);
    }

}
