package org.eclipse.openk.contactbasedata.service.util;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.viewmodel.LdapUser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.stereotype.Component;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

/**
 * Custom person attributes mapper, maps the attributes to the person POJO
 */
@Log4j2
@Component
public class LdapUserAttributesMapper implements AttributesMapper {

    /*
    Config in yaml example (variable : attributeName in LDAP):
    ----------------------
    uid: uid
    fullname: cn
    lastname: sn
    firstname: givename
    title: title
    mail: mail
    ----------------------
    */

    @Value("${ldap-sync.attribute-mapping.uid}")
    private String uid;

    @Value("${ldap-sync.attribute-mapping.fullname}")
    private String fullname;

    @Value("${ldap-sync.attribute-mapping.lastname}")
    private String lastname;

    @Value("${ldap-sync.attribute-mapping.firstname}")
    private String firstname;

    @Value("${ldap-sync.attribute-mapping.title}")
    private String title;

    @Value("${ldap-sync.attribute-mapping.department}")
    private String department;

    @Value("${ldap-sync.attribute-mapping.mail}")
    private String mail;

    @Value("${ldap-sync.attribute-mapping.telephone-number}")
    private String telephoneNumber;

    @Override
    public LdapUser mapFromAttributes(Attributes attributes) throws NamingException {
        LdapUser ldapUser = new LdapUser();

        Attribute attribute = attributes.get(uid);
        if (attribute != null) {
            ldapUser.setUid((String) attribute.get());
        }
        attribute = attributes.get(fullname);
        if (attribute != null) {
            ldapUser.setFullName((String) attribute.get());
        }
        attribute = attributes.get(lastname);
        if (attribute != null) {
            ldapUser.setLastName((String) attribute.get());
        }
        attribute = attributes.get(firstname);
        if (attribute != null) {
            ldapUser.setFirstName((String) attribute.get());
        }
        attribute = attributes.get(title);
        if (attribute != null) {
            ldapUser.setTitle((String) attribute.get());
        }
        attribute = attributes.get(department);
        if (attribute != null) {
            ldapUser.setDepartment((String) attribute.get());
        }
        attribute = attributes.get(telephoneNumber);
        if (attribute != null) {
            ldapUser.setTelephoneNumber((String) attribute.get());
        }
        attribute = attributes.get(mail);
        if (attribute != null) {
            ldapUser.setMail((String) attribute.get());
        }

        return ldapUser;
    }
}
