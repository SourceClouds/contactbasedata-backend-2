/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.contactbasedata.ContactBaseDataApplication;
import org.eclipse.openk.contactbasedata.service.ContactPersonService;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.ContactPersonDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes = ContactBaseDataApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class ContactPersonControllerTest {

    @MockBean
    private ContactPersonService contactPersonService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    void shouldReturnARequestedContactPerson() throws Exception {
        ContactPersonDto cp = MockDataHelper.mockContactPersonDto();

        when(contactPersonService.findContactPerson(any(UUID.class))).thenReturn(cp);

        mockMvc.perform(get("/contact-persons/"+UUID.randomUUID().toString()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("firstName", is(  cp.getFirstName())));
    }

    @Test
    void shouldUpdateContactPerson() throws Exception {
        ContactPersonDto cp = MockDataHelper.mockContactPersonDto();

        when( contactPersonService.updateContactPerson(any(ContactPersonDto.class))).thenReturn(cp);

        mockMvc.perform(put("/contact-persons/{uuid}", cp.getContactUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(cp)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldNotUpdateContactPersonDueToException() throws Exception {
        ContactPersonDto cp = MockDataHelper.mockContactPersonDto();

        when( contactPersonService.updateContactPerson(any(ContactPersonDto.class))).thenReturn(cp);

        // provide different uuid in url and object
        mockMvc.perform(put("/contact-persons/{uuid}", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(cp)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldInsertContactPerson() throws Exception {
        ContactPersonDto cp = MockDataHelper.mockContactPersonDto();

        when( contactPersonService.insertContactPerson(any(ContactPersonDto.class))).thenReturn(cp);

        mockMvc.perform(post("/contact-persons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(cp)))
                .andExpect(jsonPath("$.lastName", is(cp.getLastName())))
                .andExpect(jsonPath("$.contactId", not(cp.getContactUuid())))
                .andExpect(jsonPath("$.contactNote", is(cp.getContactNote())))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldDeleteAddress() throws Exception {
        mockMvc.perform(delete("/contact-persons/{contactUuid}", UUID.randomUUID(), UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }

}