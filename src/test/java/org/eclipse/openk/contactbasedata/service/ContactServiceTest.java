/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import org.eclipse.openk.contactbasedata.config.TestConfiguration;
import org.eclipse.openk.contactbasedata.model.VwDetailedContact;
import org.eclipse.openk.contactbasedata.repository.DetailedContactRepository;
import org.eclipse.openk.contactbasedata.service.util.SearchContactsFilterParams;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.when;

//@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class ContactServiceTest {
    @Qualifier("myContactService")
    @Autowired
    private ContactService contactService;

    @MockBean
    private DetailedContactRepository detailedContactRepository;

    @Test
    void shouldFindDetailedContactsProperly() {
        Page<VwDetailedContact> mockPaged = MockDataHelper.mockVDetailedContactPage();
        when(detailedContactRepository.findByFilter(isNull(), isNull(), isNull(), isNull(),
                anyBoolean(), any( Date.class), anyBoolean(), anyBoolean(), anyBoolean(), anyBoolean(), any(Pageable.class))).thenReturn(mockPaged);
        SearchContactsFilterParams filter = new SearchContactsFilterParams();
        Page<VwDetailedContact> retPage = contactService.findDetailedContacts(
                filter, PageRequest.of(0, 20));

        assertEquals(mockPaged.getTotalElements(), retPage.getTotalElements());
    }

    @Test
    void shouldFindSingleDetailedContactProperly() {
        VwDetailedContact mockContact = MockDataHelper.mockVDetailedContact();
        when(detailedContactRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(mockContact));

        VwDetailedContact detailedContact = contactService.findDetailedContactByUuid(UUID.randomUUID());

        assertEquals(detailedContact.getUuid(), mockContact.getUuid());
    }
}
