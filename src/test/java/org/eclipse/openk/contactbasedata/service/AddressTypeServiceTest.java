/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import org.eclipse.openk.contactbasedata.config.TestConfiguration;
import org.eclipse.openk.contactbasedata.model.RefAddressType;
import org.eclipse.openk.contactbasedata.repository.AddressTypeRepository;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.AddressTypeDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class AddressTypeServiceTest {
    @Qualifier("myAddressTypeService")
    @Autowired
    private AddressTypeService addressTypeService;

    @MockBean
    private AddressTypeRepository addressTypeRepository;

    @Test
    void shouldFindAllAddressTypesProperly() {
        List<RefAddressType> rs = MockDataHelper.mockRefAddressTypes();
        when(addressTypeRepository.findAll()).thenReturn(rs);
        List<AddressTypeDto> retVals = addressTypeService.findAllAddressTypes();

        assertEquals(rs.size(), retVals.size());
        assertEquals(2, rs.size());
        assertEquals(rs.get(1).getUuid(), retVals.get(1).getUuid());
    }


    @Test
    void shouldReturnAddressTypeByUuid() {

        RefAddressType refAddressType = MockDataHelper.mockRefAddressType();
        refAddressType.setType("Testtyp");
        when( addressTypeRepository.findByUuid( any(UUID.class)) ).thenReturn(Optional.of(refAddressType));

        AddressTypeDto addressTypeDto = addressTypeService.getAddressTypeByUuid(UUID.randomUUID());
        assertEquals( addressTypeDto.getUuid(), refAddressType.getUuid() );
        assertEquals( addressTypeDto.getType(), refAddressType.getType() );
    }

    @Test
    void shouldInsertAddressType(){
        RefAddressType refAddressType = MockDataHelper.mockRefAddressType();

        refAddressType.setUuid(UUID.fromString("1468275e-200b-11ea-978f-2e728ce88125"));
        refAddressType.setType("Viertadresse");
        refAddressType.setDescription("In der Wildnis");

        AddressTypeDto addressTypeDto = MockDataHelper.mockAddressTypeDto();
        addressTypeDto.setUuid(null);

        when( addressTypeRepository.save( any( RefAddressType.class) )).thenReturn(refAddressType);

        AddressTypeDto savedAddressTypeDto = addressTypeService.insertAddressType(addressTypeDto);
        assertEquals("Viertadresse", savedAddressTypeDto.getType());
        assertNotNull( savedAddressTypeDto.getUuid());
    }

    @Test
    void shouldUpdateAddressType() {

        RefAddressType addressType = MockDataHelper.mockRefAddressType();
        addressType.setType("Viertadresse");

        AddressTypeDto addressTypeDto = MockDataHelper.mockAddressTypeDto();
        addressTypeDto.setType("Kein guter Mensch");

        when( addressTypeRepository.findByUuid( any(UUID.class)) ).thenReturn(Optional.of(addressType));
        when( addressTypeRepository.save( any(RefAddressType.class)) ).thenReturn(addressType);
        AddressTypeDto addressTypeDtoUpdated = addressTypeService.updateAddressType(addressTypeDto);

        assertEquals(addressType.getUuid(), addressTypeDtoUpdated.getUuid());
        assertEquals("Viertadresse", addressTypeDtoUpdated.getType());
    }


    @Test
    void shouldDeleteAddressType() {

        RefAddressType addressType = MockDataHelper.mockRefAddressType();

        when(addressTypeRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(addressType));
        Mockito.doNothing().when(addressTypeRepository).delete( isA( RefAddressType.class ));
        addressTypeService.removeAddressType(addressType.getUuid());

        Mockito.verify(addressTypeRepository, times(1)).delete( addressType );
    }
}
