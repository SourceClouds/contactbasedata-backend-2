/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import org.eclipse.openk.contactbasedata.config.TestConfiguration;
import org.eclipse.openk.contactbasedata.exceptions.NotFoundException;
import org.eclipse.openk.contactbasedata.model.RefPersonType;
import org.eclipse.openk.contactbasedata.model.RefSalutation;
import org.eclipse.openk.contactbasedata.model.TblContact;
import org.eclipse.openk.contactbasedata.model.TblInternalPerson;
import org.eclipse.openk.contactbasedata.repository.ContactRepository;
import org.eclipse.openk.contactbasedata.repository.InternalPersonRepository;
import org.eclipse.openk.contactbasedata.repository.PersonTypeRepository;
import org.eclipse.openk.contactbasedata.repository.SalutationRepository;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.InternalPersonDto;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class InternalPersonServiceTest {
    @Qualifier("myInternalPersonService")
    @Autowired
    private InternalPersonService internalPersonService;

    @MockBean
    private InternalPersonRepository internalPersonRepository;

    @MockBean
    private ContactRepository contactRepository;

    @MockBean
    private SalutationRepository salutationRepository;

    @MockBean
    private PersonTypeRepository personTypeRepository;

    @Test
    void shouldFindAnInternalPersonProperly() {
        TblInternalPerson ip = MockDataHelper.mockInternalPerson();
        when(internalPersonRepository.findByTblContactUuid(any(UUID.class))).thenReturn(Optional.of(ip));
        InternalPersonDto dto = internalPersonService.findInternalPerson(UUID.randomUUID());

        assertEquals(ip.getContact().getUuid(), dto.getContactUuid());
    }

    @Test
    void shouldFindAllInternalPersons() {
        Page<TblInternalPerson> ipPage = MockDataHelper.mockTblInternalPersonPage();
        when(internalPersonRepository.findAll(any(Pageable.class))).thenReturn(ipPage);
        when(internalPersonRepository.findByFilter(anyString(), anyString(), any(Pageable.class))).thenReturn(ipPage);

        Page<InternalPersonDto> dtoPage = internalPersonService.findInternalPersons( true, null, null, Pageable.unpaged());
        Page<InternalPersonDto> dtoPage2= internalPersonService.findInternalPersons( false, "abc", "123", Pageable.unpaged());

        verify( internalPersonRepository, times(1)).findAll(any(Pageable.class));
        verify( internalPersonRepository, times( 1 )).findByFilter(anyString(), anyString(), any(Pageable.class));

        assertEquals( ipPage.getTotalElements(), dtoPage.getTotalElements());
        assertEquals( ipPage.getTotalElements(), dtoPage2.getTotalElements());

    }

    @Test
    void shouldThrowInErrorCaseWithInternalPerson() {
        TblInternalPerson ip = MockDataHelper.mockInternalPerson();
        when(internalPersonRepository.findByTblContactUuid(any(UUID.class))).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> internalPersonService.findInternalPerson(UUID.randomUUID()));
    }

    @Test
    void shouldUpdateInternalPerson() {
        InternalPersonDto ipDto = MockDataHelper.mockInternalPersonDto();
        TblInternalPerson ipTbl = MockDataHelper.mockInternalPerson();
        when(internalPersonRepository.findByTblContactUuid(any(UUID.class))).thenReturn(Optional.of(ipTbl));
        when(salutationRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.of(MockDataHelper.mockRefSalutation()));
        when(personTypeRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.of(MockDataHelper.mockRefPersonType()));

        when(internalPersonRepository.save(any(TblInternalPerson.class)))
                .then((Answer<TblInternalPerson>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblInternalPerson) args[0];
                });

        InternalPersonDto savedDto = internalPersonService.updateInternalPerson(ipDto);
        assertEquals(ipTbl.getContact().getUuid(), savedDto.getContactUuid());
        assertEquals(ipTbl.getContact().getNote(), savedDto.getContactNote());
        assertEquals(ipTbl.getContact().getContactType(), savedDto.getContactType());
        assertEquals(ipTbl.getFirstName(), savedDto.getFirstName());
        assertEquals(ipTbl.getLastName(), savedDto.getLastName());
        assertEquals(ipTbl.getTitle(), savedDto.getTitle());
        assertEquals(ipTbl.getUserRef(), savedDto.getUserRef());
        assertEquals(ipTbl.getDepartment(), savedDto.getDepartment());
        assertEquals(ipTbl.getUidIdent(), savedDto.getUid());
        assertEquals(ipTbl.getRefPersonType().getUuid(), savedDto.getPersonTypeUuid());
        assertEquals(ipTbl.getSalutation().getUuid(), savedDto.getSalutationUuid());
    }

    @Test
    void shouldNotUpdateInternalPerson_Exception1() {
        InternalPersonDto ipDto = MockDataHelper.mockInternalPersonDto();
        TblInternalPerson ipTbl = MockDataHelper.mockInternalPerson();
        when(internalPersonRepository.findByTblContactUuid(any(UUID.class))).thenReturn(Optional.empty());
        when(salutationRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.of(MockDataHelper.mockRefSalutation()));
        when(personTypeRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.of(MockDataHelper.mockRefPersonType()));

        when(internalPersonRepository.save(any(TblInternalPerson.class)))
                .then((Answer<TblInternalPerson>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblInternalPerson) args[0];
                });

        assertThrows( NotFoundException.class, () -> internalPersonService.updateInternalPerson(ipDto));
    }

    @Test
    void shouldNotUpdateInternalPerson_Exception2() {
        InternalPersonDto epDto = MockDataHelper.mockInternalPersonDto();
        TblInternalPerson epTbl = MockDataHelper.mockInternalPerson();
        when(internalPersonRepository.findByTblContactUuid(any(UUID.class))).thenReturn(Optional.of(epTbl));
        when(salutationRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.empty());
        when(personTypeRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.of(MockDataHelper.mockRefPersonType()));

        when(internalPersonRepository.save(any(TblInternalPerson.class)))
                .then((Answer<TblInternalPerson>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblInternalPerson) args[0];
                });

        assertThrows( NotFoundException.class, () -> internalPersonService.updateInternalPerson(epDto));
    }
    @Test
    void shouldNotUpdateInternalPerson_Exception3() {
        InternalPersonDto ipDto = MockDataHelper.mockInternalPersonDto();
        TblInternalPerson ipTbl = MockDataHelper.mockInternalPerson();
        when(internalPersonRepository.findByTblContactUuid(any(UUID.class))).thenReturn(Optional.of(ipTbl));
        when(salutationRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.of(MockDataHelper.mockRefSalutation()));
        when(personTypeRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.empty());

        when(internalPersonRepository.save(any(TblInternalPerson.class)))
                .then((Answer<TblInternalPerson>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblInternalPerson) args[0];
                });

        assertThrows( NotFoundException.class, () -> internalPersonService.updateInternalPerson(ipDto));
    }

    @Test
    void shouldInsertInternalPerson() {
        InternalPersonDto ipDto = MockDataHelper.mockInternalPersonDto();
        ipDto.setContactUuid(null);
        RefSalutation refSalutation = MockDataHelper.mockRefSalutation();
        when(salutationRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.of(refSalutation));
        RefPersonType refPersonType = MockDataHelper.mockRefPersonType();
        when(personTypeRepository.findByUuid(any(UUID.class)))
                .thenReturn(Optional.of(refPersonType));
        when(contactRepository.save(any(TblContact.class)))
                .then((Answer<TblContact>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblContact) args[0];
                });
        when(internalPersonRepository.save(any(TblInternalPerson.class)))
                .then((Answer<TblInternalPerson>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (TblInternalPerson) args[0];
                });

        InternalPersonDto savedDto = internalPersonService.insertInternalPerson(ipDto);
        assertNotNull(savedDto.getContactUuid());
        assertEquals(ipDto.getContactNote(), savedDto.getContactNote());
        assertEquals(ipDto.getContactType(), savedDto.getContactType());
        assertEquals(ipDto.getFirstName(), savedDto.getFirstName());
        assertEquals(ipDto.getLastName(), savedDto.getLastName());
        assertEquals(ipDto.getTitle(), savedDto.getTitle());
        assertEquals(refPersonType.getUuid(), savedDto.getPersonTypeUuid());
        assertEquals(refSalutation.getUuid(), savedDto.getSalutationUuid());
    }
}