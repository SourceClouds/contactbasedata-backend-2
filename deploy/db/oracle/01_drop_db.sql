﻿-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2019 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------

DROP VIEW VW_DETAILED_CONTACT;
DROP VIEW VW_GENERAL_CONTACT;

DROP TABLE TBL_ASSIGNMENT_MODUL_CONTACT;
DROP SEQUENCE TBL_ASSIGN_MODUL_CNTCT_ID_SEQ;

DROP TABLE TBL_INTERNAL_PERSON;
DROP SEQUENCE TBL_INTERNAL_PERSON_ID_SEQ;

DROP TABLE TBL_EXTERNAL_PERSON;
DROP SEQUENCE TBL_EXTERNAL_PERSON_ID_SEQ;

DROP TABLE TBL_CONTACT_PERSON;
DROP SEQUENCE TBL_CONTACT_PERSON_ID_SEQ;

DROP TABLE TBL_COMPANY;
DROP SEQUENCE TBL_COMPANY_ID_SEQ;

DROP TABLE TBL_COMMUNICATION;
DROP SEQUENCE TBL_COMMUNICATION_ID_SEQ;

DROP TABLE TBL_ADDRESS;
DROP SEQUENCE TBL_ADDRESS_ID_SEQ;

DROP TABLE REF_SALUTATION;
DROP SEQUENCE REF_SALUTATION_ID_SEQ;

DROP TABLE REF_COMMUNICATION_TYPE;
DROP SEQUENCE REF_COMMUNICATION_TYPE_ID_SEQ;

DROP TABLE REF_PERSON_TYPE;
DROP SEQUENCE REF_PERSON_TYPE_ID_SEQ;

DROP TABLE REF_ADDRESS_TYPE;
DROP SEQUENCE REF_ADDRESS_TYPE_ID_SEQ;

DROP TABLE TBL_CONTACT;
DROP SEQUENCE TBL_CONTACT_ID_SEQ;

DROP TABLE VERSION;
